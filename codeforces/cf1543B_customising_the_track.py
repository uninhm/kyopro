# uninhm
# https://codeforces.com/contest/1543/problem/B
# greedy

for _ in range(int(input())):
    n = int(input())
    a = list(map(int, input().split()))
    s = sum(a)
    x = s - s//n*n
    print(x * (n-x))
