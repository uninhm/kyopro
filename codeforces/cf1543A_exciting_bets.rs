// uninhm
// https://codeforces.com/contest/1543/problem/A
// greedy, math, modulo

use std::io::stdin;
use std::cmp::min;

fn main() {
    let mut line = String::new();
    stdin().read_line(&mut line).unwrap();

    let t: i64 = line.trim().parse().unwrap();

    for _ in 0 .. t {
        line.clear();
        stdin().read_line(&mut line).unwrap();

        let words: Vec<i64> = line
            .split_whitespace()
            .map(|x| x.parse().unwrap())
            .collect();

        let a = words[0];
        let b = words[1];

        let m = min(a, b);
        let d = i64::abs(a - b);

        if d == 0 {
            println!("0 0");
            continue;
        }
        
        println!("{} {}", d, min(m%d, d-m%d));
    }
}
